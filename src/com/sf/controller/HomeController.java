package com.sf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class HomeController {
   
    @RequestMapping("/home")   
    public String homePage(ModelMap model) {
        return "home";
    }
    
    @RequestMapping("/cloudservice")
    public String cloudService(ModelMap m)
    {
    	return "cloudservice";
    }
    
    @RequestMapping("/jefencase")
    public String jefencase()
    {
    	return "jefencase";
    }
    
    @RequestMapping("/mobileservice")
    public String mobileservice()
    {
    	return "mobileservice";
    }
    
    @RequestMapping("/o2o")
    public String o2oIntroduction()
    {
    	return "o2o";
    }
    
    @RequestMapping("/aboutus")
    public String about()
    {
    	return "aboutus";
    }
    
    @RequestMapping("/recruitment")
    public String recruitment()
    {
    	return "recruitment";
    }    
}