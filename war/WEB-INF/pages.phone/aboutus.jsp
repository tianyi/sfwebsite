<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn">
  <head>
  	<title>联系我们</title>
    <!-- base header -->
    <%@include file="base_header.jsp" %>
    
  </head>
	<body>	  	
	  <!-- NAVBAR -->
	  <%@include file="navbar.jsp" %>

	  <div class="container marketing">
	  
	  <hr class="featurette-divider_top">
	  
	  <div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>联系我们</div>
		   	<br>
		  	</div>
  	</div>
	  

	  
	  <div class="row featurette">
		<div class="col-md-4">
			<img class="featurette-image img-responsive" alt="Generic placeholder image" src="images/aboutus1.jpg" style="min-height: 226px;">
		</div>
		<div class="col-md-8">
			<img class="featurette-image img-responsive" alt="Generic placeholder image" src="images/aboutus2.jpg">
		</div>
	  </div>
	  <hr class="featurette-divider_sf">
	  
	  <div class="row featurette">
		<div class="col-md-12">
			<p class="lead">地址：北京市朝阳区慈云寺北里210号远洋国际中心E座28层</p>
	         <p class="lead">电话：010-65980858-844</p>
	         <p class="lead">邮箱：x.chang@doublev2v.com</p>
	         
		</div>
	  </div>
	  
	   <div class="row">
        	<div class="col-md-12">
        		<p>扫描关注微信公众号: shuangfengwangluo<br><img class="sf_weixin" src="images/sf_weixin.jpg"></p>
        	</div>
        	
        </div>
	  
	  <hr class="featurette-divider_sf">
	  
      <!-- FOOTER -->
      <%@include file="footer.jsp" %>
      
      </div>	  

      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<%@include file="base_js.jsp" %>
	</body>
	</html>
