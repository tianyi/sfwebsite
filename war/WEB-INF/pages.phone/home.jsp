<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn">
  <head>
    <!-- base header -->
    <%@include file="base_header.jsp" %>
    
    <title>首页</title>
    
  </head>
  <body>
 
  	
  	<!-- NAVBAR -->
    <%@include file="navbar.jsp" %>
    
    <!-- Carousel ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="images/home_o2o1.jpg" alt="First slide">
         
        </div>
        <div class="item">
          <img src="images/home_cloud.jpg" alt="Second slide">
         
        </div>
        <div class="item">
          <img src="images/home_mobile.jpg" alt="Third slide">
         
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->

    <div class="container">
   
    	<div class="row">
    	
    		<div class="col-md-6 col-xs-12">
    			<div class="row">
    				<div class="col-md-12 col-xs-12">
    					<br>
    					<p class="span" style="font-weight:bold; font-size: 20px">我们提供的服务</p>
    				</div>
    			</div>
    			<div class="row">
    				<div class="col-md-4 col-xs-12 ">
	    				<h4>云服务</h4>
			    		<p><em>-----降低企业成本</em></p>
			    		<p class="span font_home_desc"><a href="cloudservice">我们拥有成熟的云计算开发平台。为客户提供综合的云服务解决方案，
		    									包括基础架构云、云数据中心、移动应用中间件，分布式计算架构等。减少客户在在软件、硬件、运营方面成本。</a>
		    			</p>
	    			</div>
	    			<div class="col-md-4 col-xs-12 ">
	    				<h4>O2O解决方案</h4>
			    		<p><em>-----助力企业腾飞</em></p>
			    		<p class="span font_home_desc"><a href="o2o">移动互联网的兴起，带动了O2O领域的蓬勃发展。
			    		        我们根据客户的不同需求，为客户提供量身定制的O2O解决方案，综合软件、硬件、云服务、
			    		        移动服务等，将一体化的服务整体呈现给我们的客户。</a>
			    		</p>
	    			</div>
	    			<div class="col-md-4 col-xs-12 ">
	    				<h4>移动服务</h4>
			    		<p><em>-----提高企业效率</em></p>
			    		<p class="span font_home_desc"><a href="mobileservice">智能终端平台的发展，带给了企业新的急需解决的业务需求，
			    		将业务、办公甚至运营部署到移动终端上面。目前，移动服务已经成为了企业战略布局，
			    		快速抢占市场的手段。</a>
		
			    		</p>
	    			</div>
    			</div>
    			
    		</div>
    		
    		<div class="col-md-6 col-xs-12">
    			<div class="row">
    				<div class="col-md-6 col-xs-12">
	    				<br>
	    				<p class="span" style="font-weight:bold; font-size: 20px">吉芬案例展示</p>
    				</div>
    			</div>
    			<div class="row">
    				<div class="col-md-6 col-xs-12">
    					<a href="jefencase"><img class="featurette-image img-responsive" src="images/jefencase_demo.jpg" alt="jefen case demo" ></a>
    				</div>
    			</div>
    		</div>
    	</div>
    
	 
   		<!-- 
   			<embed 
   				src="http://d1.sina.com.cn/201410/29/579013_mondeo_sina_topbanner_1030.swf" 
   				allowFullScreen="true" 
   				quality="high" 
   				width="455" 
   				height="250" 
   				align="middle" 
   				allowScriptAccess="always" 
   				type="application/x-shockwave-flash">
   			</embed>
   			-->
	    		
    </div>
    <hr class="featurette-divider_sf" style="margin-top: 30px">

 	<div class="container">
 		<!-- Wrap the rest of the page in another container to center all the content. -->
      
      <!-- FOOTER -->
      <%@include file="footer.jsp" %>
	</div> <!-- end container -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <%@include file="base_js.jsp" %>
  </body>
</html>