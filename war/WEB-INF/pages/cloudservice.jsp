<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn">
  <head>
  	<title>云服务</title>
    <!-- base header -->
    <%@include file="base_header.jsp" %>
  </head>
  <body>
  	<!-- NAVBAR -->
    <%@include file="navbar.jsp" %>
    
  	<div class="container marketing">
  	<hr class="featurette-divider_top">
  	<div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>云服务简介</div>
		   	<br>
		  	</div>
  	</div>
  	
  	<div class="row featurette">
	  	
	  	<div class="col-md-12">
	   		<p class="text_indent">网络传输速度的加速提升、移动互联网的快速发展，带动了云计算技术的革新。而云计算正在以前所未有的方式影响着企业的运营。</p>
	   		<p class="text_indent">移动互联网的发展造就了企业千变万化的云计算需求。双丰网络为企业客户提供数据、业务流程、移动应用和IT资源的集成服务，为企业客户提供量身定制的云服务解决方案。</p>
	   		<p class="text_indent">我们通过合作与集成，创造全新的合作模式，同合作伙伴一起为最终用户创造价值。</p>
	  	</div>
  	</div>
  	<div class="row featurette">
	  	<div class="col-md-3"></div>
	  	<div class="col-md-6">
	  		<img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="images/cloud_service1.png">
	  		<img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="images/cloud_service2.png">
	  	</div>
	  	<div class="col-md-3"></div>
  	</div>
  	
      <!-- START THE FEATURETTES -->
      <hr class="featurette-divider_sf">
      <div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>服务范围</div>
		   	<br>
		  	</div>
  	</div>
  	

	  
	  <div class="row featurette">
	  	<div class="col-md-3 col-xs-3">
	  		<div class="well well_placeholder_panel" style="background-color: #F0E68C; overflow:hidden; "></div>
	  	</div>
	  	<div class="col-md-3 col-xs-3">
	  		<div class="well well_placeholder_panel" style="background-color: #F08080; overflow:hidden; "></div>
	  	</div>
	  	<div class="col-md-3 col-xs-3">
	  		<div class="well well_placeholder_panel" style="background-color: #87CEFA; overflow:hidden; "></div>
	  	</div>
	  	<div class="col-md-3 col-xs-3">
	  		<div class="well well_placeholder_panel" style="background-color: #87CEFA; overflow:hidden; "></div>
	  	</div>
	  </div>
	  
	  <div class="row featurette">
	  	<div class="col-md-3 col-xs-3">
	  		<div class="well_panel_title">
		      		
		      		<div><h4 class="lead bold_text_align">云服务架构</h4></div>		      		
	  		</div>
	  	</div>
	  	<div class="col-md-3 col-xs-3">
	  		<div class="well_panel_title">
		      		 
		      		<div><h4 class="lead bold_text_align">移动应用开发</h4></div>	      		
	  		</div>
	  	</div>
	  	<div class="col-md-3 col-xs-3">
	  		<div class="well_panel_title">
	      		
	      		<div><h4 class="lead bold_text_align">云数据中心</h4></div>
		      			      		
	  		</div>
	  	</div>
	  	<div class="col-md-3 col-xs-3">
	  		<div class="well_panel_title">
	      		
	      		<div><h4 class="lead bold_text_align">IT集成服务</h4></div>
		      			      		
	  		</div>
	  	</div>
	  </div>
	  
      <div class="row featurette">
      	<div class="col-md-3 col-xs-3">
	      	<div class="well well_panel_cloud" style="background-color: #F0E68C; overflow:hidden; ">
		 		<div >
		 		<p class="text_indent">
		 			双丰网络综合运用当前多种优势技术，创建出来一整套云服务架构。 通过分析客户的需求，为客户提供灵活的IT解决方案，达到快速调整从而适应客户不断变化的需求。
		 		</p>
		      	</div>
	      	</div>
	     </div>
	      	
	     <div class="col-md-3 col-xs-3">
	      	<div class="well well_panel_cloud" style="background-color: #F08080; ">
				<div>
				<p class="text_indent">
				根据客户的特定需求，开发出科学，完善的基于云计算的移动应用，帮助客户打造一体化云服务终端，提升客户的服务水平。
				</p>
		      	</div>
	      	</div>
	      </div>
	      	
	      <div class="col-md-3 col-xs-3">
      		<div class="well well_panel_cloud" style="background-color: #87CEFA; ">
		      	<div>
		      	<p class="text_indent">
		      	为客户提供高速、稳定、负载均衡的云服务数据中心，帮助客户节省数据中心的维护、部署等运营费用。
		      	</p>
		      	</div>
	      	</div>
	      </div>
	      
	      <div class="col-md-3 col-xs-3">
      		<div class="well well_panel_cloud" style="background-color: #87CEFA; ">
		      	<div>
		      	<p class="text_indent">
		      	在传统互联网时代，企业都积累了一些IT资源，如：ERP、OA等。双丰网络通过提供云服务的方式，为客户提供IT资源的集成服务，帮助客户真正实现SaaS（软件即服务）。
		      	</p>
		      	</div>
	      	</div>
	      </div>
      </div>
      
      <div class="row featurette">
	  	<!-- <div class="col-md-3"></div> -->
	  	<div class="col-md-12">
	  		<img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="images/cloud_our_service1.png">
	  		<img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="images/cloud_our_service2.png">
	  	</div>
	  	<!-- <div class="col-md-3"></div> -->
  	</div>
      
      <hr class="featurette-divider_sf">
      <div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>我们的优势</div>
		   	<br>
		  	</div>
  	</div>
	 <div class="row featurette">
      	<div class="col-md-12">
	      	<div class="well" style="background-color: #F0E68C; ">
		      	<div class="center_div">
		      		<div style="float:left;">
		      			<img class="featurette-image img-responsive"  alt="Generic placeholder image" src="images/zan.png" height="40" width="40">
		      			
		      		</div> 
		      		<div>
			      		<h4 class="lead" style="font-weight:bold">高度集成的一体化服务</h4>
		      		</div>
		      			      		
		      	</div>
		      	<div><hr class="gray_hr"></div>
		      		<div><p class="text_indent">双丰网络将数据中心、基础云架构、移动应用开发高度集中起来，为客户提供流畅的一体化服务。我们拥有丰富的云服务架构经验，复杂均衡的集群数据库方案，快速的移动应用开发团队。</p>
		      	</div>
	      	</div>

	      	
      		<div class="well" style="background-color: #87CEFA; ">
		      	<div class="center_div">
		      		<div style="float:left;">
		      			<img class="featurette-image img-responsive"  alt="Generic placeholder image" src="images/zan.png" height="40" width="40">
		      		</div> 
		      		<div><h4 class="lead" style="font-weight:bold">安全可靠的数据中心服务</h4></div>
		      			      		
		      	</div>
		      	<div><hr class="gray_hr"></div>
		      		<div><p class="text_indent">互联网时代的数据安全，日益重要，双丰网络拥有致力于为客户打造安全可靠的数据中心，确保客户的数据安全。保证客户的业务运营的流畅运作。</p>
		      	</div>
	      	</div>
	      	
	      	<div class="well" style="background-color: #90EE90; ">
		      	<div class="center_div">
		      		<div style="float:left;">
		      			<img class="featurette-image img-responsive"  alt="Generic placeholder image" src="images/zan.png" height="40" width="40">
		      		</div> 
		      		<div><h4 class="lead" style="font-weight:bold">快速的移动应用开发服务 </h4></div>
		      			      		
		      	</div>
		      	<div><hr class="gray_hr"></div>
		      	<div>
		      		<p class="text_indent">在互联网时代，移动应用的需求越来越个性化，需求变化速度越来越快。双丰网络的快速移动客户端开发能力，能确保满足客户的不断变化的移动客户端开发需求。</p>
		      	</div>
	      	</div>
	      	
      	</div>
      </div>

  <!--     <div class="row featurette">
      	<div class="col-md-12">
        	<h2></h2>
        </div>
        <div class="col-md-4">
        	<p class="lead">丰富的行业经验与业务咨询能力</p>
        	<img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="images/cloud.png">
        </div>
        <div class="col-md-4">
        	<p class="lead">高度兼容的平台底层虚拟化模块</p>
        	<img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="images/cloud.png">
        </div>
        <div class="col-md-4">
          <p class="lead">专业先进的支持系统</p>
          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="images/cloud.png">
        </div>
     
      </div> -->

      <hr class="featurette-divider_sf">

      <!-- FOOTER -->
      <%@include file="footer.jsp" %>
      
    </div><!-- /.container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <%@include file="base_js.jsp" %>
  </body>
</html>