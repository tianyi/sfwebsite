<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn">
  <head>
  	<title>招聘信息</title>
    <!-- base header -->
    <%@include file="base_header.jsp" %>

  </head>
	<body>	  	
	  <!-- NAVBAR -->
	  <%@include file="navbar.jsp" %>
	  
	  <div class="container marketing">
	  

	  <hr class="featurette-divider_top">

  	  <div class="row featurette">
	  	<div class="col-md-12">
	  	<div><img class="star_star" alt="" src="images/star.png"></div>
	   	<div class=star_title>招聘信息</div>
	   	<br>
	  	</div>
 	  </div>
  	
	<div class="row featurette">
		<div class="col-md-12">
			<img class="featurette-image img-responsive" alt="Generic placeholder image" src="images/recruitment.jpg">
		</div>
	</div>
  	
  	<div class="row featurette">
				<div class="col-md-12">
				
	
			
	<hr class="featurette-divider_sf">
					
<div class="mod position">
  <div class="hd"> 
  		<h3 id="position-ydcpkfandroid">Android开发工程师</h3>
  </div>
</div>

<p>简历发送至：x.chang@doublev2v.com</p>
					    
<em>职位描述:</em>
<pre>
1. 基于Android系统的智能手机和平板电脑设备的应用(偏向于电商类应用)
</pre>
  
  
<em>职位要求:</em>
<pre>
1. 至少经历过一个完整的Android应用产品开发
2. 熟悉Android系统以及SDK
3. 精通Java，熟悉多线程编程和面向对象编程
4. 喜欢钻研新技术
5. 思维活跃，善于发现问题，更善于解决问题 
</pre>
  
<em>具有下列条件者优先:</em>
<pre>
1. 有html5开发经验者
2. 自己独立开发过应用
</pre>
					    
</div>

	</div> 
	
	<hr class="featurette-divider_sf">
      
      <!-- FOOTER -->
      <%@include file="footer.jsp" %>
  
	  	     </div>  
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<%@include file="base_js.jsp" %>
	</body>
	</html>
