<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>


<!-- NAVBAR ================================================== -->

    <!-- NAVBAR ================================================ -->
    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <!-- <a class="navbar-brand" href="#">Project name</a>  -->
              <a class="navbar-brand" href=home><img class="navbar-brand_sf" src="images/logo.png"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
              
              <% 
            	String url11 = request.getServletPath();
            	String url_path = url11.substring(url11.lastIndexOf("/")+1, url11.lastIndexOf(".")); 
            	if(url_path.equalsIgnoreCase("HOME"))
            	{
            		out.println("<li class=\"active\"><a href=\"home\">首页</a></li>");
            		out.println("<li><a href=\"cloudservice\">云服务</a></li>");
            		out.println("<li><a href=\"mobileservice\">移动服务</a></li>");
            		
            		out.println("<li><a href=\"o2o\">O2O解决方案</a></li>");
            		out.println("<li><a href=\"jefencase\">吉芬成功案例</a></li>");
            	}else if(url_path.equalsIgnoreCase("cloudservice"))
            	{
            		out.println("<li><a href=\"home\">首页</a></li>");
            		out.println("<li class=\"active\"><a href=\"cloudservice\">云服务</a></li>");
            		out.println("<li><a href=\"mobileservice\">移动服务</a></li>");
            		
            		out.println("<li><a href=\"o2o\">O2O解决方案</a></li>");
            		out.println("<li><a href=\"jefencase\">吉芬成功案例</a></li>");
            	}else if(url_path.equalsIgnoreCase("mobileservice"))
            	{
            		out.println("<li><a href=\"home\">首页</a></li>");
            		out.println("<li><a href=\"cloudservice\">云服务</a></li>");
            		out.println("<li class=\"active\"><a href=\"mobileservice\">移动服务</a></li>");
            		
            		out.println("<li><a href=\"o2o\">O2O解决方案</a></li>");
            		out.println("<li><a href=\"jefencase\">吉芬成功案例</a></li>");
            	}else if(url_path.equalsIgnoreCase("jefencase"))
            	{
            		out.println("<li><a href=\"home\">首页</a></li>");
            		out.println("<li><a href=\"cloudservice\">云服务</a></li>");
            		out.println("<li><a href=\"mobileservice\">移动服务</a></li>");
            		
            		out.println("<li><a href=\"o2o\">O2O解决方案</a></li>");
            		out.println("<li class=\"active\"><a href=\"jefencase\">吉芬成功案例</a></li>");
            	}else if(url_path.equalsIgnoreCase("o2o"))
            	{
            		out.println("<li><a href=\"home\">首页</a></li>");
            		out.println("<li><a href=\"cloudservice\">云服务</a></li>");
            		out.println("<li><a href=\"mobileservice\">移动服务</a></li>");
            		
            		out.println("<li class=\"active\"><a href=\"o2o\">O2O解决方案</a></li>");
            		out.println("<li><a href=\"jefencase\">吉芬成功案例</a></li>");
            	}
            	else
            	{
            		out.println("<li class=\"active\"><a href=\"home\">首页</a></li>");
            		out.println("<li><a href=\"cloudservice\">云服务</a></li>");
            		out.println("<li><a href=\"mobileservice\">移动服务</a></li>");
            		
            		out.println("<li><a href=\"o2o\">O2O解决方案</a></li>");
            		out.println("<li><a href=\"jefencase\">吉芬成功案例</a></li>");
            	}
            	
              %>
              
                <!-- out.println("");  -->
                
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
    