<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn">
  <head>
  	<title>吉芬成功案例</title>
    <!-- base header -->
    <%@include file="base_header.jsp" %>
  </head>
<body>
	<!-- NAVBAR -->
    <%@include file="navbar.jsp" %>
    
  	<div class="container marketing">
      <!-- START THE FEATURETTES -->
      <hr class="featurette-divider_top">
      
  
	  
	  <div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>项目背景</div>
		   	<br>
		  	</div>
  	</div>
	  
	  <div class="row featurette">
	  		
	      <div class="col-md-12">
		  	<img class="featurette-image img-responsive" alt="Generic placeholder image" src="images/jefencase_baner_first.jpg"> 		
		  </div>
	  </div>
      
      <div class="row featurette">
        <div class="col-md-12">
        		<p><br></p>
	          <p class="text_indent">我们的客户是一家致力于为28至40岁风华正茂的女性所设计的高贵服装。它以时尚、高贵、独特的文化内涵引导人们高品位的生活方式，通过时尚的款式设计，精良的手工制做工艺及中西合璧、无与伦比的板型制造所带来的穿着效果，充分体现穿着该品牌所带给人们的风采与自豪，该品牌已经在海内外树立起极高的知名度与美誉度。在设计中，于简约的设计风范之中融入了诸多时尚流行元素，体现了设计师对于时装、艺术和流行文化的观察与思考。
	该客户在国内外市场建立起自己品牌之后，与迅速占领市场，主打中自信、知性、优雅的知识女性的高级时装市场，希望在近三年内实现国内市场份额达到预期的占有率。
	在互联网的推动下，纯粹线下实体店的营销渠道不能满足市场的需要，无法达到低成本推广、高效率运营的要求，随着O2O模式兴起和发展，改变营销模式势在必行。
			  </p>
        </div>
      </div>
      
      <hr class="featurette-divider_sf">
      
   
	  
	  <div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>服务能力</div>
		   	<br>
		  	</div>
  	</div>


      <div class="row featurette">
        
        <div class="col-md-12">
          <p class="text_indent">云衣橱 （消费者）: 消费者可以线上预约到JEFEN店铺量体、人像采集、选择面料，JEFEN安排专业的设计顾问为您提供一对一的尊贵服务！
                                                                                       消费者可以线上预约订单，选择设计师或作品，实现与设计师点对点沟通，及时高效，操作便捷。</p>
          <p class="text_indent">JEFEN高端定制系统（店铺端）: JEFEN 会员来店购物可以在专业设计顾问的推荐下，定制您所需的高品质服装。
                                 JEFEN安排了专业的顾问为您量体。旗舰店划分了专门的服务区域，实现自动人像采集，敬请期待！</p>
          <p class="text_indent">JEFEN高端定制系统（设计师）: 基于海量客户数据、设计作品、组合搭配，设计师可以根据用户的年龄、职业特性、体型、喜好等，
                                                                                         从大数据和经验值中挖掘到近似款，实现对客户的精准推送，提高了设计效率，同时降低了客单价。</p>
        </div>
      </div>
      
      
      
      <div class="row featurette">
        
        <div class="col-md-12">
        	<img class="featurette-image img-responsive" alt="Generic placeholder image" src="images/jefencase_service1.jpg">
        	<img class="featurette-image img-responsive" alt="Generic placeholder image" src="images/jefencase_service2.jpg">
        	
        	<p><br></p>
        </div>
      </div>
      
      <hr class="featurette-divider_sf">
      
	  
	  <div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>服务内容</div>
		   	<br>
		  	</div>
  	</div>
	  
	  <div class="row featurette">
	  	<div class="col-md-12">
          <p class="lead text_lead">资源整合</p>
	          <p class="text_indent">
	          	根据客户价值为其提供差别化的产品和服务，并努力与客户建立长期合作的战略伙伴关系。将线下资源和线上资源进行共享，充分利用现有资源。
	          </p>
	      <p class="lead text_lead">展示延伸</p>
	          <p class="text_indent">
	          	将公司现有的更多产品放到线上平台进行宣传，对产品的特性可以进行详细的介绍，提供了更多展示的可能，降低营销成本，增加客户参与度。
	          </p>
	      <p class="lead text_lead">消费活力</p>
	          <p class="text_indent">
	          	通过O2O平台，客户能够了解到更的产品，使得客户更能找到自己喜欢的产品，并通过客户间的评价参与等进行口碑宣传，粉丝用户可以带动多人参与。
	          </p>
	      <p class="lead text_lead">管理升级</p>
	          <p class="text_indent">
	          	O2O商城管理系统，将购买的商品进行线下服务，提升客户的满意度，同时也能提高订单的准确性和效率。
	          </p>
	  	</div>
	  </div>
	  
	  <div class="row featurette">
	      <div class="col-md-12">
		  	<img class="featurette-image img-responsive" alt="Generic placeholder image" src="images/jefencase_baner_last.jpg"> 		
		  </div>
	  	
	  </div>

      <hr class="featurette-divider_sf">

    
      
      <!-- FOOTER -->
      <%@include file="footer.jsp" %>
      
    </div><!-- /.container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <%@include file="base_js.jsp" %>
</body>
</html>