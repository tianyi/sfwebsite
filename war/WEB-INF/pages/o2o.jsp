<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn">
  <head>
  	<title>O2O解决方案</title>
    <!-- base header -->
    <%@include file="base_header.jsp" %>
  </head>
<body>
	<!-- NAVBAR -->
    <%@include file="navbar.jsp" %>
    
  	<div class="container marketing">
      <!-- START THE FEATURETTES -->
      <hr class="featurette-divider_top">
    
	  
	  <div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>O2O商业模式</div>
		   	<br>
		  	</div>
  	    </div>
      
      <div class="row featurette">
      	<div class="col-md-12">
      		<img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="images/o2o_baner.jpg">
      	</div>
      </div>
      
      <div class="row featurette">
      	<div class="col-md-12">
	      	<p><br></p>
      		<p class="text_indent">
      		O2O商业模式是利用移动互联网技术贯通线上信息和线下服务，为客户提供更加丰富便捷的信息服务和优质的线下服务体验的新兴商业模式;
借助o2o平台便捷的信息服务，客户可以获得更丰富、全面的商家及其服务的内容信息、
更加便捷的商家在线咨询和预售;
通过O2O平台，商家能够获得更多的产品宣传展示机会吸引更多新客户到店消费。
掌握用户数据，大大提升对老客户的维护与营销效果。
      		
          </p>
      	</div>
      </div>
      
      <hr class="featurette-divider_sf">
  
	  
	  <div class="row featurette">
		  	<div class="col-md-12 col-xs-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>服务范围</div>
		   	<br>
		  	</div>
  	    </div>
	  
	  <div class="row featurette">
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well well_placeholder_panel" style="background-color: #F0E68C; overflow:hidden; "></div>
	  	</div>
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well well_placeholder_panel" style="background-color: #F08080; overflow:hidden; "></div>
	  	</div>
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well well_placeholder_panel" style="background-color: #87CEFA; overflow:hidden; "></div>
	  	</div>
	  </div>
	  
	  <div class="row featurette">
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well_panel_title">
		      		
		      		<div><h4 class="lead bold_text_align">O2O平台的搭建与维护</h4></div>		      		
	  		</div>
	  	</div>
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well_panel_title">
		      		
		      		<div><h4 class="lead bold_text_align">O2O平台数据分析</h4></div>	      		
	  		</div>
	  	</div>
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well_panel_title">
	      		
	      		<div><h4 class="lead bold_text_align">O2O平台运营的支持</h4></div>
		      			      		
	  		</div>
	  	</div>
	  </div>
	  
      <div class="row featurette">
      	<div class="col-md-4 col-xs-4">
	      	<div class="well well_panel" style="background-color: #F0E68C; overflow:hidden; ">
		 		<div >
		 		<p class="text_indent">
		 			基于双丰网络专有的技术，为合作企业提供O2O平台前期的的搭建服务，协助客户制定运营方案。O2O平台运营过程中，为其提供日常维护服务和技术支持。
		 		</p>
		      	</div>
	      	</div>
	     </div>
	      	
	     <div class="col-md-4 col-xs-4">
	      	<div class="well well_panel" style="background-color: #F08080; ">
				<div>
				<p class="text_indent">
				对平台日常产生的数据进行收集整理，分析客户行为，建立相关分析工具，
				使得平台运营方可以对客户的消费习惯、消费能力，行为偏好等进行分析、预测，为其做出合理的运营规划提供数据支撑。
				</p>
		      	</div>
	      	</div>
	      </div>
	      	
	      <div class="col-md-4 col-xs-4">
      		<div class="well well_panel" style="background-color: #87CEFA; ">
		      	<div>
		      	<p class="text_indent">
		      	为O2O平台运营方提供日常运营的建议，提高客户的活跃度，
		      	为商家提供营销方面的技术支持，增加客户的成交量；通过线上宣传、线下体验等营销手段，增加客户的信任度，提升自身品牌的知名度。
		      	</p>
		      	</div>
	      	</div>
	      </div>
      </div>
      
      <hr class="featurette-divider_sf">

	  
	  <div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>我们的优势</div>
		   	<br>
		  	</div>
  	    </div>
	  <!-- 
	  <div class="row featurette">
	      <div class="col-md-12">
		  	<img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="images/cross_hands.jpg"> 		
		  </div>
	  </div>
	  <div>
	  	<p><br></p>
	  </div>
	   -->
	  
	  <div class="row featurette"  >
	  	<div class="col-md-12" >
	  		<div class="row sf_row_margin" style="background: #F0E68C">
		      	<div class="col-md-3 text_all_align" style="text-align:center;">
			      	<h3>专业的技术团队</h3>
		      	</div>
		      	<div class="col-md-9 text_all_align">
		      		<p class="">我们有专业的技术团队，为O2O平台制定专门的方案.依靠强大的专业
                                                                                          知识和高素质的专家团队为企业提供定制式服务，满足企业O2O营销的各种需要。
		      		</p>
		      	</div>
	      	</div>
	      	
	      	<div class="row">
	      		<div class="col-md-12">
			      	<p><br></p>
		      	</div>
	      	</div>
		     
		  		<div class="row sf_row_margin" style="background: #87CEFA">
			      	<div class="col-md-3 text_all_align" style="text-align:center;">
				      	<h3>创新的设计理念</h3>
			      	</div>
			      	<div class="col-md-9 text_all_align" >
			      		<p class="">我们的团队善于创新、勇于创新，不仅有前沿的技术创新，更有革新的流程创新、思维创新。为企业提供新鲜健康的发展血液。</p>
			      	</div>
		      	</div>
		      	
		      	<div class="row">
	      		<div class="col-md-12">
			      	<p><br></p>
		      	</div>
	      	</div>
		     
		  		<div class="row sf_row_margin" style="background: #90EE90">
			      	<div class="col-md-3 text_all_align" style="text-align:center;">
				      	<h3>周到的客户服务</h3>
			      	</div>
			      	<div class="col-md-9 text_all_align">
			      		<p class="">我们不仅会提供适合企业自身发展和市场需要的针对性强的个性服务，使得产品能够适应市场的需求，
			      		                                提高客户的满意度，提升企业的品牌；而且会为企业进行数据分析、目标客户的挖掘和客户资源的整理提供全程化的服务。</p>
			      	</div>
		      	</div>
	     </div>
	  </div>
	  
	

      <hr class="featurette-divider_sf">
      
      <!-- FOOTER -->
      <%@include file="footer.jsp" %>
      
    </div><!-- /.container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <%@include file="base_js.jsp" %>
</body>
</html>