<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn">
  <head>
  	<title>移动服务</title>
    <!-- base header -->
    <%@include file="base_header.jsp" %>

  </head>
	<body>	  	
	  <!-- NAVBAR -->
	  <%@include file="navbar.jsp" %>
	  
	  
	  
	  <div class="container marketing">
	  
      	<hr class="featurette-divider_top">
      	<div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>移动服务</div>
		   	<br>
		  	</div>
  	    </div>
  	  
  	  <div class="row featurette">
	  	<div class="col-md-12">
	   		<p class="text_indent">
	   		
	   			双丰网络移动服务致力于为移动应用开发提供技术和服务支持，降低移动应用开发难度，缩短移动应用开发周期。
我们的后端云服务平台整合了绝大部分常用的服务端操作，简化了服务器搭建和服务端程序开发过程。为了解决移动应用跨平台的问题，我们提供基于HTML5，CSS，JavaScript等标准Web技术的开发框架，同时对接后端云服务平台提供的API服务，实现跨Android，IOS，WindowsPhone等主流移动平台的应用开发能力。我们提供的应用定制服务，根据用户业务需求，将业务流程整合到移动应用中，并添加社交、支付等常用移动应用功能扩展。

             </p>
	  	</div>
  	  </div>
	  	<div class="row featurette">
	  		<div class="col-md-3"></div>
		  	<div class="col-md-6">
		   		<img class="featurette-image img-responsive" alt="Generic placeholder image" src="images/mobileservice.png">
		  	</div>
		  	<div class="col-md-3"></div>
	  	</div>
      <hr class="featurette-divider_sf">
      
  	  
  	  <div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>服务范围</div>
		   	<br>
		  	</div>
  	    </div>


      <div class="row featurette blue_sf" style="margin-right: 0px;margin-left: 0px;" >
	  	<div class="col-md-12" >
	  		<div class="row blue_title_panel_sf" ></div>
	  		<div class="row line_service1_sf">
		      	<div class="col-md-3 text_all_align" style="text-align:center;">
			      	<h3>BaaS后端云服务</h3>
		      	</div>
		      	<div class="col-md-9 text_all_align line_service2_sf">
		      		<p class="">为用户提供数据、文件存储等基础服务，同时集成账户管理、消息推送、社交网络整合、搜索、地理位置与广告等常用功能。采用API接口方式供用户调用，同时提供云端后台管理系统，简化了服务端构建过程。</p>
		      	</div>
	      	</div>
		    <div class="row line_service1_sf">
			     <div class="col-md-12" >
			  		<div class="row">
				      	<div class="col-md-3 text_all_align" style="text-align:center;">
					      	<h3>移动应用开发框架</h3>
				      	</div>
				      	<div class="col-md-9 text_all_align line_service2_sf" >
				      		<p class="">提供基于HTML5等web标准技术的移动应用开发框架，帮助开发者开发跨平台的移动应用程序。所开发的应用可在Android，IOS，WindowsPhone等主流移动平台运行。框架同时封装了对后端服务API接口的调用。</p>
				      	</div>
			      	</div>
			     </div>
		     </div>
		     
		     <div class="row ">
			     <div class="col-md-12 " >
			  		<div class="row ">
				      	<div class="col-md-3 text_all_align" style="text-align:center;">
					      	<h3>应用定制服务</h3>
				      	</div>
				      	<div class="col-md-9 text_all_align line_service2_sf">
				      		<p class="">根据用户提供的业务需求和业务流程，为用户设计开发移动应用。利用已有的云服务和开发框架支持，快速高质量低成本的为用户开发定制化移动应用。</p>
				      	</div>
			      	</div>
			     </div>
		     </div>
	     </div>
	  </div>
      
     
  
      
	<hr class="featurette-divider_sf" style="margin-top: 35px;">
	
	<div class="row featurette">
		  	<div class="col-md-12">
		  	<div><img class="star_star" alt="" src="images/star.png"></div>
		   	<div class=star_title>我们的优势</div>
		   	<br>
		  	</div>
  	    </div>
  	    

      
	  <div class="row featurette">
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well well_placeholder_panel" style="background-color: #F0E68C; overflow:hidden; "></div>
	  	</div>
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well well_placeholder_panel" style="background-color: #F08080; overflow:hidden; "></div>
	  	</div>
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well well_placeholder_panel" style="background-color: #87CEFA; overflow:hidden; "></div>
	  	</div>
	  </div>
	  <div class="row featurette">
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well_panel_title">
	      		 
	      		<div><h4 class="lead bold_text_align">强大的云服务支持</h4></div>
	  		</div>
	  	</div>
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well_panel_title">
	      		
	      		<div><h4 class="lead bold_text_align">完整的开发体验</h4></div>
	  		</div>
	  	</div>
	  	<div class="col-md-4 col-xs-4">
	  		<div class="well_panel_title">
	      		
	      		<div><h4 class="lead bold_text_align">优秀的技术团队</h4></div>
	  		</div>
	  	</div>
	  </div>
	  
	  
	  
      <div class="row featurette">
      	<div class="col-md-4 col-xs-4">
	      	<div class="well well_panel" style="background-color: #F0E68C; overflow:hidden; ">
		 		<div ><p class="text_indent">后端云服务支持，提供了从用户管理、消息管理、存储管理到安全管理等全方位的服务端功能，为移动应用的开发提供了强大支撑和坚实基础。</p>
		      	</div>
	      	</div>
	     </div>
	     <div class="col-md-4 col-xs-4">
	      	<div class="well well_panel" style="background-color: #F08080; ">
				<div><p class="text_indent">我们的开发框架能为用户提供跨平台的应用开发方式，同时整合后端服务API，为用户提供从服务端到应用端的完整开发体验。</p>
		      	</div>
	      	</div>
	      </div>
	      <div class="col-md-4 col-xs-4">
      		<div class="well well_panel" style="background-color: #87CEFA; ">
		      	<div><p class="text_indent">拥有一支技术过硬、有丰富行业经验的专家队伍，为用户提供优质的专业咨询、技术支持、项目实施等服务，确保服务的高质量。</p>
		      	</div>
	      	</div>
	      </div>
      </div>
	
      <hr class="featurette-divider_sf">
      
      <!-- FOOTER -->
      <%@include file="footer.jsp" %>
      
      </div>	  
	  	        
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<%@include file="base_js.jsp" %>
	</body>